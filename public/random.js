/*This JavaScrip-class sets the zoom-position in the images
to random-values every 1.5 sec an passes them to style.css */

let root = document.documentElement;

function changePositions() {
    root.style.setProperty('--posX', getRandomPosition());
    root.style.setProperty('--posY', getRandomPosition());
}

function getRandomPosition() {
    let randomPosition = Math.random() * 100;
    return randomPosition + '%';
}

setInterval(changePositions, 1500);

